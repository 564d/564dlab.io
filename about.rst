---
title: About
---
😀 Hello, and welcome to my website! My name is 564d and I am a student who loves technologies. I created this website to improve my skills, as well as to share my thoughts and insights on various topics.

I use GitLab as my preferred platform for version control, collaboration, and continuous integration. GitLab is a web-based service that allows me to create, manage, and share code repositories with other developers. It also offers a range of features such as issue tracking, code review, testing, deployment, and more.

On this website, you will find links to my GitLab account, where you can browse through some of my public repositories and see what I have been working on lately. I hope you enjoy reading them!

